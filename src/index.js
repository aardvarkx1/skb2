import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';
import './task3a';

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
    console.log(pc);
  })
  .catch(err => {
    console.log('Чтото пошло не так:', err);
  });

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task3A/', (req, res) => {
  console.log(req.originalUrl);
  let array = req.originalUrl.split('/').slice(2);
  let query = array[0];

  for (var i = 1; i < array.length; i++) {
    query += '.';
    query += array[i];
  }
  console.log(query);
  if (!query) {
    return res.json(pc)
  } else {
    console.log('IN ELSE');
    return res.json(pc[query])
  }
});

app.get('/task3A/board', (req, res) => {
  return res.json(pc.board);
});

app.get('/task3A/board/vendor', (req, res) => {
  return res.json(pc.board.vendor);
});

app.get('/task3A/board/model', (req, res) => {
  return res.json(pc.board.model);
});

app.get('/task3A/board/cpu', (req, res) => {
  return res.json(pc.board.cpu);
});

app.get('/task3A/board/image', (req, res) => {
  return res.json(pc.board.image);
});

app.get('/task3A/board/video', (req, res) => {
  return res.json(pc.board.video);
});

app.get('/task3A/ram', (req, res) => {
  return res.json(pc.ram);
});

app.get('/task3A/ram/vendor', (req, res) => {
  return res.json(pc.ram.vendor);
});

app.get('/task3A/ram/volume', (req, res) => {
  return res.json(pc.ram.volume);
});

app.get('/task3A/ram/pins', (req, res) => {
  return res.json(pc.ram.pins);
});

app.get('/task3A/os', (req, res) => {
  return res.json(pc.os);
});

app.get('/task3A/hdd', (req, res) => {
  return res.json(pc.hdd);
});

app.get('/task2A', (req, res) => {
  const sum = (parseInt(req.query.a) || 0) + (parseInt(req.query.b) || 0);
  res.send(sum.toString());
});

app.get('/task2B', (req, res) => {
  const fio = req.query.fullname.split(' ');
  if (fio.length > 3 || fio === '') {
    res.send('Invalid fullname');
  } else if (fio.length === 3) {
    res.send(fio[2].trim() + ' ' + fio[0].trim()[0] + '. ' + fio[1].trim()[0] + '.');
  } else if (fio.length === 2) {
    res.send(fio[1].trim() + ' ' + fio[0].trim()[0] + '.');
  } else {
    res.send(fio[0].trim());
  }
});

app.get('/task2C', (req, res) => {
  const username = req.query.username.split('/').slice(-1)[0].split('?')[0];
  // console.log(typeof input[0]);
  // const username = type(input)
  if (username[0] === '@') {
    res.send(username);
  } else {
    res.send('@' + username);
  }
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
